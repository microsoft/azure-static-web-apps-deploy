#!/bin/bash

Params=""

if [ ! -z "$VERBOSE" ]
then
    Params="$Params --verbose $VERBOSE"
fi
if [ ! -z "$WORKING_DIR" ]
then 
	Params="$Params --workdir $WORKING_DIR"
fi
if [ ! -z "$BUILD_TIMEOUT_IN_MIN" ]
then
	Params="$Params --buildTimeoutInMinutes $BUILD_TIMEOUT_IN_MIN"
fi
if [ ! -z "$APP_LOCATION" ]
then
	Params="$Params --app $APP_LOCATION"
fi
if [ ! -z "$API_LOCATION" ]
then
	Params="$Params --api $API_LOCATION"
fi
if [ ! -z "$ROUTES_LOCATION" ]
then
	Params="$Params --routesLocation $ROUTES_LOCATION"
fi
if [ ! -z "$CONFIGFILE_LOCATION" ]
then
	Params="$Params --configFileLocation $CONFIGFILE_LOCATION"
fi	
if [ ! -z "$OUTPUT_LOCATION" ]
then
	Params="$Params --outputLocation $OUTPUT_LOCATION"
fi
if [ ! -z "$DATA_API_LOCATION" ]
then
	Params="$Params --dataApiLocation $DATA_API_LOCATION"
fi
if [ ! -z "$ARTIFACT_LOCATION" ]
then
    Params="$Params --appArtifactLocation $ARTIFACT_LOCATION"
fi
if [ ! -z "$EVENT_PATH" ]
then
    Params="$Params --event $EVENT_PATH"
fi
if [ ! -z "$REPO_TOKEN" ]
then
    Params="$Params --repoToken $REPO_TOKEN"
fi
if [ ! -z "$APP_BUILD_COMMAND" ]
then
	Params="$Params --appBuildCommand $APP_BUILD_COMMAND"
fi
if [ ! -z "$API_BUILD_COMMAND" ]
then
	Params="$Params --apiBuildCommand $API_BUILD_COMMAND"
fi
if [ ! -z "$REPO_URL" ]
then
	Params="$Params --repositoryUrl $REPO_URL"
fi
if [ ! -z "$DEPLOYMENT_ENV" ]
then
	Params="$Params --deploymentEnvironment $DEPLOYMENT_ENV"
fi
if [ ! -z "$BRANCH" ]
then
	Params="$Params --branch $BRANCH"
fi
if [ ! -z "$SKIP_APP_BUILD" ]
then
	Params="$Params --skipAppBuild $SKIP_APP_BUILD"
fi
if [ ! -z "$SKIP_API_BUILD" ]
then
	Params="$Params --skipApiBuild $SKIP_API_BUILD"
fi
if [ ! -z "$IS_STATIC_EXPORT" ]
then 
	Params="$Params --isStaticExport $IS_STATIC_EXPORT"
fi
if [ ! -z "$PR_TITLE" ]
then
	Params="$Params --pullRequestTitle $PR_TITLE"
fi
if [ ! -z "$HEAD_BRANCH" ]
then
	Params="$Params --headBranch $HEAD_BRANCH"
fi
if [ ! -z "$PROD_BRANCH" ]
then
	Params="$Params --productionBranch $PROD_BRANCH"
fi

cd /bin/staticsites/
./StaticSitesClient --deploymentaction 'upload' --deploymentProvider 'BitBucket' --apiToken $API_TOKEN "$Params"